$(document).ready(function(){
    
    $('#agree').click(function(){
        $('#checkbox').attr('checked', true);
        var isChecked=  $('#checkbox').attr('checked');

        if(isChecked == 'checked'){
             $("#fieldset").removeAttr('disabled');
        }
    });
    
    $('#submit').click(function(){ 
        var fName = $('#first').val();
        var lName = $('#last').val();
        var fnamePattern = /^[A-Z][a-z]{1,20}$/;
        
        var lnamePattern = /^[A-Z][a-z]{1,20}|[A-Z][a-z][[A-Z][a-z]{1,20}|[A-Z][a-z]{1,10} [A-Z][a-z]{1,10} ([A-Z][a-z]{1,10})?|[A-Z]'[A-Z][a-z]{1,20}$/;
        
        var email = $('#email').val();
        var emailPattern = /^[A-Za-z0-9_\-\.]{1,20}@[A-Za-z0-9\-]{1,20}\.[a-zA-Z]{2,4}$/;
        
        var phone = $('#tele').val();
        var phonePattern = /^\([0-9]{3}\) [0-9]{3}\-[0-9]{4}$/;
        
        var city = $("#city").val();
        var cityPattern = /^([A-Z][a-z]{1,20})|([A-z]{1}[a-z]{1,10} [A-Z][a-z]{1,20})$/;
        
        var state = $("#state").val();
        var statePattern = /^ $/;
        
        var zip = $('#zip').val();
        var zipPattern = /^[0-9]{5}(-[0-9]{4})?$/;
        
        var error="";
        var check = $('#checkbox').is(":checked");
        
        if (!fnamePattern.test(fName)){
            error += "Error First Name\n";
        }
        
        if (!lnamePattern.test(lName)){
            error += "Error Last Name\n";
        }
        
        if (!emailPattern.test(email)){
            error += "Error Email\n";
        }
        
        if(!phonePattern.test(phone)){
            error += "Error Phone Number\n";
        }
        
        if(!cityPattern.test(city)){
            error += "Error City\n";
        }
        
        if(statePattern.test(state)){
            error += "Error pick a State\n";
        }
        
        if(!zipPattern.test(zip)){
            error += "Error Zip Code\n";
        }
        
        if(!check){
            error += "Error Agree to Terms of Conditions\n";
        }
        
        if(error != ""){
            alert("Please fix these errors:\n"+error);
            return false;
        }
            
    });

});