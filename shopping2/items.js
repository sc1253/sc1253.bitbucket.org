$(document).ready(function(){
    
    $('#carousel1').carousel({
        interval: false
    }); 
    $('#carousel2').carousel({
        interval: false
    }); 
    $('#carousel3').carousel({
        interval: false
    }); 
    $('#carousel4').carousel({
        interval: false
    }); 
    $('#carousel5').carousel({
        interval: false
    }); 
    $('#carousel6').carousel({
        interval: false
    }); 
    $('#carousel7').carousel({
        interval: false
    }); 
    $('#carousel8').carousel({
        interval: false
    }); 
    $('#carousel9').carousel({
        interval: false
    }); 
    $('#carousel10').carousel({
        interval: false
    }); 
    
    var array=[];
    var count =0;
    var subtotal = 0.00;
    var tax =0.00;
    var total =0;
    var shipping =5.73;
    
    
    $('#soaker').click(function(){ 
         array[count] = 1;
         count++;
         $('.cartItems').append("<div class='row'> <div class='col-xs-2'><img src='Final_Photos/super_soaker1.jpg' align='left'></div> <div class='col-xs-6'> Super Soaker</div> <div class='col-xs-4'>$80.00</div> </div>");
         subtotal += 80.00;
         $('.subtotal'). text("$"+subtotal.toFixed(2));
         tax = subtotal * .07;
         $('.tax').text("$"+tax.toFixed(2));
         $('.shipping').text("$"+shipping.toFixed(2));
         total = subtotal + tax + shipping;
         $('.total').text("$"+total.toFixed(2));
         $('.amount').append('<input type="hidden" name="amount" value="' + total.toFixed(2)+'">');
     });
    
    $('#tama').click(function(){ 
         array[count] = 2;
         count++;
         $('.cartItems').append("<div class='row'> <div class='col-xs-2'><img src='Final_Photos/tomogachi1.jpg' align='left'></div> <div class='col-xs-6'> Tamagotchi</div> <div class='col-xs-4'>$42.00</div> </div>");
         subtotal += 42.00;
         $('.subtotal'). text("$"+subtotal.toFixed(2));
         tax = subtotal * .07;
         $('.tax').text("$"+tax.toFixed(2));
         $('.shipping').text("$"+shipping.toFixed(2));
         total = subtotal + tax + shipping;
         $('.total').text("$"+total.toFixed(2));
         $('.amount').append('<input type="hidden" name="amount" value="' + total.toFixed(2)+'">');
     });
     
     $('#furby').click(function(){ 
         array[count] = 3;
         count++;
         $('.cartItems').append("<div class='row'> <div class='col-xs-2'><img src='Final_Photos/furby1.jpg' align='left'></div> <div class='col-xs-6'> Furby</div> <div class='col-xs-4'>$25.00</div> </div>");
         subtotal += 25.00;
         $('.subtotal'). text("$"+subtotal.toFixed(2));
         tax = subtotal * .07;
         $('.tax').text("$"+tax.toFixed(2));
         $('.shipping').text("$"+shipping.toFixed(2));
         total = subtotal + tax + shipping;
         $('.total').text("$"+total.toFixed(2));
         $('.amount').append('<input type="hidden" name="amount" value="' + total.toFixed(2)+'">');
     });
     
     $('#bopit').click(function(){ 
         array[count] = 4;
         count++;
         $('.cartItems').append("<div class='row'> <div class='col-xs-2'><img src='Final_Photos/bopit1.jpg' align='left'></div> <div class='col-xs-6'> Bop It!</div> <div class='col-xs-4'>$14.00</div> </div>");
         subtotal += 14.00;
         $('.subtotal'). text("$"+subtotal.toFixed(2));
         tax = subtotal * .07;
         $('.tax').text("$"+tax.toFixed(2));
         $('.shipping').text("$"+shipping.toFixed(2));
         total = subtotal + tax + shipping;
         $('.total').text("$"+total.toFixed(2));
         $('.amount').append('<input type="hidden" name="amount" value="' + total.toFixed(2)+'">');
     });
     
     $('#polly').click(function(){ 
         array[count] = 5;
         count++;
         $('.cartItems').append("<div class='row'> <div class='col-xs-2'><img src='Final_Photos/polly1.jpg' align='left'></div> <div class='col-xs-6'> Polly Pocket</div> <div class='col-xs-4'>$16.00</div> </div>");
         subtotal += 16.00;
         $('.subtotal'). text("$"+subtotal.toFixed(2));
         tax = subtotal * .07;
         $('.tax').text("$"+tax.toFixed(2));
         $('.shipping').text("$"+shipping.toFixed(2));
         total = subtotal + tax + shipping;
         $('.total').text("$"+total.toFixed(2));
         $('.amount').append('<input type="hidden" name="amount" value="' + total.toFixed(2)+'">');
     });
     
     $('#moon').click(function(){ 
         array[count] = 6;
         count++;
         $('.cartItems').append("<div class='row'> <div class='col-xs-2'><img src='Final_Photos/moon1.jpg' align='left'></div> <div class='col-xs-6'> Moon Shoes</div> <div class='col-xs-4'>$25.00</div> </div>");
         subtotal += 25.00;
         $('.subtotal'). text("$"+subtotal.toFixed(2));
         tax = subtotal * .07;
         $('.tax').text("$"+tax.toFixed(2));
         $('.shipping').text("$"+shipping.toFixed(2));
         total = subtotal + tax + shipping;
         $('.total').text("$"+total.toFixed(2));
         $('.amount').append('<input type="hidden" name="amount" value="' + total.toFixed(2)+'">');
     });
     
     $('#trolls').click(function(){ 
         array[count] = 7;
         count++;
         $('.cartItems').append("<div class='row'> <div class='col-xs-2'><img src='Final_Photos/trolls2.jpg' align='left'></div> <div class='col-xs-6'> Treasure Trolls</div> <div class='col-xs-4'>$ 5.00</div> </div>");
         subtotal += 5.00;
         $('.subtotal'). text("$"+subtotal.toFixed(2));
         tax = subtotal * .07;
         $('.tax').text("$"+tax.toFixed(2));
         $('.shipping').text("$"+shipping.toFixed(2));
         total = subtotal + tax + shipping;
         $('.total').text("$"+total.toFixed(2));
         $('.amount').append('<input type="hidden" name="amount" value="' + total.toFixed(2)+'">');
     });
     $('#creepy').click(function(){ 
         array[count] = 8;
         count++;
         $('.cartItems').append("<div class='row'> <div class='col-xs-2'><img src='Final_Photos/creepy1.jpg' align='left'></div> <div class='col-xs-6'> Creepy Crawlers</div> <div class='col-xs-4'>$30.00</div> </div>");
         subtotal += 30.00;
         $('.subtotal'). text("$"+subtotal.toFixed(2));
         tax = subtotal * .07;
         $('.tax').text("$"+tax.toFixed(2));
         $('.shipping').text("$"+shipping.toFixed(2));
         total = subtotal + tax + shipping;
         $('.total').text("$"+total.toFixed(2));
         $('.amount').append('<input type="hidden" name="amount" value="' + total.toFixed(2)+'">');
     });
     
     $('#elmo').click(function(){ 
         array[count] = 9;
         count++;
         $('.cartItems').append("<div class='row'> <div class='col-xs-2'><img src='Final_Photos/elmo.jpg' align='left'></div> <div class='col-xs-6'> Tickle Me Elmo</div> <div class='col-xs-4'>$30.00</div> </div>");
         subtotal += 30.00;
         $('.subtotal'). text("$"+subtotal.toFixed(2));
         tax = subtotal * .07;
         $('.tax').text("$"+tax.toFixed(2));
         $('.shipping').text("$"+shipping.toFixed(2));
         total = subtotal + tax + shipping;
         $('.total').text("$"+total.toFixed(2));
         $('.amount').append('<input type="hidden" name="amount" value="' + total.toFixed(2)+'">');
     });
    
    $('#gak').click(function(){ 
        array[count] = 10;
        count++;
        $('.cartItems').append("<div class='row'> <div class='col-xs-2'><img src='Final_Photos/gak1.jpg' align='left'></div> <div class='col-xs-6'> Gak</div> <div class='col-xs-4'>$ 7.00</div> </div>");
        subtotal += 7.00;
         $('.subtotal'). text("$"+subtotal.toFixed(2));
         tax = subtotal * .07;
         $('.tax').text("$"+tax.toFixed(2));
         $('.shipping').text("$"+shipping.toFixed(2));
         total = subtotal + tax + shipping;
         $('.total').text("$"+total.toFixed(2));
         $('.amount').append('<input type="hidden" name="amount" value="' + total.toFixed(2)+'">');
    });
    
});